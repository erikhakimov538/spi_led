     /* адрес порта и смещения */
     .equ   OE,             0x04         /* направление порта */
     .equ   FUNC,           0x08         /* режим работы порта */
     .equ   ANALOG,         0x0C         /* аналоговый режим работы порта */
     .equ   PULL,           0x10         /* подтяжка порта */
     .equ   PD,             0x14         /* режим работы выходного драйвера */
     .equ   PWR,            0x18         /* режим мощности передатчика */
     .equ   GFEN,           0x1C         /* режим работы входного фильтра */

     /* значения для OE */
     .equ PORT_OE_IN,           0x0
     .equ PORT_OE_OUT,          0x1
     /* значения для FUNC */
     .equ   PORT_FUNC_PORT,     0x0
     .equ   PORT_FUNC_MAIN,     0x1
     .equ   PORT_FUNC_ALTER,    0x2
     .equ   PORT_FUNC_OVERRID,  0x3
     /* значения для OE */
     .equ   PORT_MODE_ANALOG,   0x0
     .equ   PORT_MODE_DIGITAL,  0x1
     /* значения для подтяжек (PULL) */
     .equ   PORT_PULL_DOWN_OFF, 0x0
     .equ   PORT_PULL_DOWN_ON,  0x1
     .equ   PORT_PULL_UP_OFF,   0x0
     .equ   PORT_PULL_UP_ON,    0x1
    /* значения для режима работы  */
     .equ   PORT_PD_DRIVER,     0x0
     .equ   PORT_PD_OPEN,       0x1
     .equ   PORT_PD_SHM_OFF,    0x0
     .equ   PORT_PD_SHM_ON,     0x1
    /* значения для PWR */
     .equ   PORT_PWR_RESERVED,  0x0
     .equ   PORT_PWR_SLOW,      0x1
     .equ   PORT_PWR_FAST,      0x2
     .equ   PORT_PWR_MAX_FAST,  0x3
    /* значения для GFEN */
     .equ   PORT_GFEN_OFF,      0x0
     .equ   PORT_GFEN_ON,       0x1
     
